#!/bin/bash
CHANGELOG="$(date +%Y%m%d%H%M)"
kubectl create configmap httpgo-data-cm -n http-ns --from-file=static/ -o yaml --dry-run=client > httpgo-data-cm.yaml
kubectl apply -f httpgo-data-cm.yaml
git commit -am "$CHANGELOG rescan 'statíc' directory"