FROM golang:alpine3.11 AS builder

RUN apk update && apk add --no-cache git

ENV USER=appuser
ENV UID=10001 

RUN adduser \    
    --disabled-password \    
    --gecos "" \    
    --home "/nonexistent" \    
    --shell "/sbin/nologin" \    
    --no-create-home \    
    --uid "${UID}" \    
    "${USER}"
RUN mkdir /static
COPY main.go /
WORKDIR /
RUN go get -d -v
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -a -o /main .
RUN chmod +x /main

###########

FROM scratch

COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group
COPY --from=builder /static /static
COPY --from=builder /main /

USER appuser:appuser

ENTRYPOINT ["/main"]
EXPOSE 8080
