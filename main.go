package main

import (
	"log"
	"net/http"
)

func main() {
	fs := http.FileServer(http.Dir("./static"))
	http.Handle("/", fs)

	listenport := ":8080"

	log.Println("Listening on ", listenport, "...")
	err := http.ListenAndServe(listenport, nil)
	if err != nil {
		log.Fatal(err)
	}
}
